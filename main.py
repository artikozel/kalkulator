import calc_helper

def get_float(data):
    while True:
        try:
            return float(input(data))
        except:
            print("To nie liczba. Spróbuj ponownie.")

if __name__ == '__main__':
    while True:
        print("Podaj dwie liczby: ")
        a = get_float("Podaj liczbę a: ")
        b = get_float("Podaj liczbę b: ")
        while True:
            print("Jaką operację chcesz wykonać?")
            print("1. Dodaj")
            print("2. Odejmij")
            print("3. Pomnóż")
            print("4. Podziel")
            print("0. Wyjdź")
            choice = get_float("... ")
            if choice == 1:
                print(calc_helper.add(a, b))
            elif choice == 2:
                print(calc_helper.subtract(a, b))
            elif choice == 3:
                print(calc_helper.multiply(a, b))
            elif choice == 4:
                print(calc_helper.divide(a, b))
            elif choice >= 5:
                print("Opcja niedostępna")
            if choice == 0:
                print("Wychodzę z programu")
                break
        if choice == 0:
            break