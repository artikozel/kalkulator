def add(a, b):
    return a + b

def subtract(a, b):
    result = a - b
    return result

def multiply(a, b):
    result = a * b
    return result

def divide(a, b):
    if a == 0:
        return "0"
    elif b == 0:
        return "Nie dziel przez 0!"
    else:
        return a / b
